Scénář workshopu LaTeX pro začátečníky 2023 (FIT++)
===================================================

1. Pracovat budeme v cloud editoru https://www.papeeria.com/ v Live Demo.
2. Zkompilujeme demo dokument (Papeeria Live demo) a prohlédneme náhled. Popíšeme části zdrojáku. Ukážeme možnosti kompilace a přepneme na `TeX Live 2019` a `XeLaTeX`.
3. Vložíme obsah souboru `ukazka-strukturovany_text.tex`, zkompilujeme, popíšeme části zdrojáku a jak se projevují na výstupu v náhledu.
4. Doplnit podporu češtiny přes `polyglossia` a `xevlna`.
5. Promítnu `vysledek-strukturovany_text.pdf`, studenti samostatně zkusí dojít k tomu stejnému.
6. Vložíme obsah souboru `ukazka-matematika.tex`, společně projdeme a vysvětlíme. Zdůrazním odkaz na seznam symbolů v komentáři.
7. Promítnu `vysledek-matematika.pdf`, studenti pracují samostatně.
8. Promítnout slajdy DPR-LaTeX s tím, co instalovat (TeXLive). Asistovat při instalaci. Jako test zkusit z CLI `xelatex` a to, co stahneme z Papeerie.
9. Promítnout slajdy DPR-LaTeX s editory. Nechat studenty si něco rozběhat.
10. Promítnout slajdy DPR-LaTeX s dalšími zdroji ke čtení
